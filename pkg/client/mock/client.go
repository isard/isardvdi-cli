package mock

import (
	"context"
	"net/url"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

type Client struct {
	mock.Mock
}

var ensureMock client.Interface = &Client{}

func (c *Client) SetBeforeRequestHook(hook func(*client.Client) error) {
	c.Called(hook)
}

func (c *Client) URL() *url.URL {
	return nil
}

func (c *Client) Version(ctx context.Context) (string, error) {
	args := c.Called(ctx)
	return args.String(0), args.Error(1)
}

func (c *Client) Maintenance(ctx context.Context) (bool, error) {
	args := c.Called(ctx)
	return args.Bool(0), args.Error(1)
}

func (c *Client) SetToken(tkn string) {
	c.Called(tkn)
}

func (c *Client) AuthForm(ctx context.Context, category, usr, pwd string) (string, error) {
	args := c.Called(ctx, category, usr, pwd)
	return args.String(0), args.Error(1)
}

func (c *Client) UserVPN(ctx context.Context) (string, error) {
	args := c.Called(ctx)
	return args.String(0), args.Error(1)
}

func (c *Client) UserOwnsDesktop(ctx context.Context, opts *client.UserOwnsDesktopOpts) error {
	args := c.Called(ctx, opts)
	return args.Error(0)
}

func (c *Client) AdminUserList(ctx context.Context) ([]*client.User, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.User), args.Error(1)
}

func (c *Client) AdminUserCreate(ctx context.Context, provider, role, category, group, uid, username, pwd string) (*client.User, error) {
	args := c.Called(ctx, provider, role, category, group, uid, username, pwd)
	return args.Get(0).(*client.User), args.Error(1)
}

func (c *Client) AdminUserDelete(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}

func (c *Client) AdminGroupCreate(ctx context.Context, category, uid, name, description, externalAppID, externalGID string) (*client.Group, error) {
	args := c.Called(ctx, category, uid, name, description, externalAppID, externalGID)
	return args.Get(0).(*client.Group), args.Error(1)
}

func (c *Client) AdminDesktopList(ctx context.Context) ([]*client.AdminDesktop, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.AdminDesktop), args.Error(1)
}

func (c *Client) AdminTemplateList(ctx context.Context) ([]*client.Template, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.Template), args.Error(1)
}

func (c *Client) AdminHypervisorUpdate(ctx context.Context, hyp *client.Hypervisor) error {
	args := c.Called(ctx, hyp)
	return args.Error(0)
}

func (c *Client) AdminHypervisorOnlyForced(ctx context.Context, id string, onlyForced bool) error {
	args := c.Called(ctx, id, onlyForced)
	return args.Error(0)
}

func (c *Client) HypervisorList(ctx context.Context) ([]*client.Hypervisor, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.Hypervisor), args.Error(1)
}

func (c *Client) HypervisorGet(ctx context.Context, id string) (*client.Hypervisor, error) {
	args := c.Called(ctx, id)
	return args.Get(0).(*client.Hypervisor), args.Error(1)
}

func (c *Client) DesktopList(ctx context.Context) ([]*client.Desktop, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.Desktop), args.Error(1)
}

func (c *Client) DesktopGet(ctx context.Context, id string) (*client.Desktop, error) {
	args := c.Called(ctx, id)
	return args.Get(0).(*client.Desktop), args.Error(1)
}

func (c *Client) DesktopCreate(ctx context.Context, name, templateID string) (*client.Desktop, error) {
	args := c.Called(ctx, name, templateID)
	return args.Get(0).(*client.Desktop), args.Error(1)
}

func (c *Client) DesktopCreateFromScratch(ctx context.Context, name, xml string) (*client.Desktop, error) {
	args := c.Called(ctx, name, xml)
	return args.Get(0).(*client.Desktop), args.Error(1)
}

func (c *Client) DesktopUpdate(ctx context.Context, id string, opts client.DesktopUpdateOptions) error {
	args := c.Called(ctx, id, opts)
	return args.Error(0)
}

func (c *Client) DesktopDelete(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}

func (c *Client) DesktopStart(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}

func (c *Client) DesktopStop(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}

func (c *Client) DesktopViewer(ctx context.Context, t client.DesktopViewer, id string) (string, error) {
	args := c.Called(ctx, t, id)
	return args.String(0), args.Error(0)
}

func (c *Client) TemplateList(ctx context.Context) ([]*client.Template, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.Template), args.Error(1)
}

func (c *Client) TemplateCreateFromDesktop(ctx context.Context, name, desktopID string) (*client.Template, error) {
	args := c.Called(ctx, name, desktopID)
	return args.Get(0).(*client.Template), args.Error(1)
}

func (c *Client) StatsCategoryList(ctx context.Context) ([]*client.StatsCategory, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.StatsCategory), args.Error(1)
}

func (c *Client) StatsDeploymentByCategory(ctx context.Context) ([]*client.StatsDeploymentByCategory, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.StatsDeploymentByCategory), args.Error(1)
}

func (c *Client) StatsUsers(ctx context.Context) ([]*client.StatsUser, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.StatsUser), args.Error(1)
}

func (c *Client) StatsDesktops(ctx context.Context) ([]*client.StatsDesktop, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.StatsDesktop), args.Error(1)
}

func (c *Client) StatsTemplates(ctx context.Context) ([]*client.StatsTemplate, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.StatsTemplate), args.Error(1)
}

func (c *Client) StatsHypervisors(ctx context.Context) ([]*client.StatsHypervisor, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.StatsHypervisor), args.Error(1)
}

func (c *Client) OrchestratorHypervisorList(ctx context.Context) ([]*client.OrchestratorHypervisor, error) {
	args := c.Called(ctx)
	return args.Get(0).([]*client.OrchestratorHypervisor), args.Error(1)
}
func (c *Client) OrchestratorHypervisorGet(ctx context.Context, id string) (*client.OrchestratorHypervisor, error) {
	args := c.Called(ctx, id)
	return args.Get(0).(*client.OrchestratorHypervisor), args.Error(1)
}
func (c *Client) OrchestratorHypervisorManage(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}
func (c *Client) OrchestratorHypervisorUnmanage(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}
func (c *Client) OrchestratorHypervisorAddToDeadRow(ctx context.Context, id string) (time.Time, error) {
	args := c.Called(ctx, id)
	return args.Get(0).(time.Time), args.Error(1)
}
func (c *Client) OrchestratorHypervisorRemoveFromDeadRow(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}
func (c *Client) OrchestratorHypervisorStopDesktops(ctx context.Context, id string) error {
	args := c.Called(ctx, id)
	return args.Error(0)
}
