package cfg

import (
	"errors"
	"log"
	"strings"

	"github.com/spf13/viper"
)

type Cfg struct {
	Token       string
	Host        string
	IgnoreCerts bool `mapstructure:"ignore_certs"`
}

func Init(cfg *Cfg) {
	viper.SetConfigName("cli")

	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME/.isardvdi")
	viper.AddConfigPath("$HOME/.config/isardvdi")
	viper.AddConfigPath("/etc/isardvdi")

	viper.SetDefault("token", "")
	viper.SetDefault("host", "")
	viper.SetDefault("ignore_certs", false)

	viper.SetEnvPrefix("ISARDVDI_CLI")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		if !errors.Is(err, viper.ConfigFileNotFoundError{}) {
			log.Fatalf("read configuration: %v", err)
		}
	}

	if err := viper.Unmarshal(cfg); err != nil {
		log.Fatalf("unmarshal configuration: %v", err)
	}
}

func Save(cfg *Cfg) error {
	viper.Set("token", cfg.Token)
	viper.Set("host", cfg.Host)
	viper.Set("ignore_certs", cfg.IgnoreCerts)

	return viper.WriteConfig()
}
