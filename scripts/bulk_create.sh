#!/usr/bin/env bash

TEMPLATE_ID="_local-default-admin-admin-Debian_9.5.0"
NAME_TEMPLATE="Debian_CLI"

created_desktops=$(go run main.go desktop list | awk '{ print $4 }')
for i in {1..100}; do
    name="${NAME_TEMPLATE}_${i}"

    if [ -n "$(echo "$created_desktops" | grep "$name")" ]; then
	echo "$name already created"
    else
	go run main.go desktop create --name "$name" --from-template "$TEMPLATE_ID"
    fi
done

list_stopped() {
    go run main.go desktop list | grep "$NAME_TEMPLATE" | grep -e "Stopped" -e "Failed" | awk '{ print $2 }'
}

stopped=$(list_stopped)
while [ -n "$stopped" ]; do
    echo "$stopped" | xargs -I{} go run main.go desktop start --id '{}'
    stopped=$(list_stopped)
done
