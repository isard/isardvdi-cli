module gitlab.com/isard/isardvdi-cli

go 1.16

require (
	github.com/google/go-querystring v1.1.0
	github.com/olekukonko/tablewriter v0.0.6-0.20210304033056-74c60be0ef68
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1
)
