package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"

	"github.com/olekukonko/tablewriter"
)

var adminDesktopListCmd = &cobra.Command{
	Use:   "list",
	Short: "list all the desktops",
	RunE: func(cmd *cobra.Command, args []string) error {
		desktops, err := cli.AdminDesktopList(cmd.Context())
		if err != nil {
			return err
		}

		if outputJSON {
			return encodeJSON(desktops)
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name", "State", "User", "Hypervisor Started"})

		for _, d := range desktops {
			table.Append([]string{client.GetString(d.ID), client.GetString(d.Name), client.GetString(d.State), client.GetString(d.User), client.GetString(d.HypervisorStarted)})
		}

		table.Render()

		return nil
	},
}
