package cmd

import (
	"os"

	"gitlab.com/isard/isardvdi-cli/pkg/client"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

var desktopListCmd = &cobra.Command{
	Use:   "list",
	Short: "list all the desktops of the user",
	RunE: func(cmd *cobra.Command, args []string) error {
		desktops, err := cli.DesktopList(cmd.Context())
		if err != nil {
			return err
		}

		if outputJSON {
			return encodeJSON(desktops)
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name", "State"})

		for _, d := range desktops {
			table.Append([]string{client.GetString(d.ID), client.GetString(d.Name), client.GetString(d.State)})
		}

		table.Render()

		return nil
	},
}
