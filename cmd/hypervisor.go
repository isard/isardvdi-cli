package cmd

import "github.com/spf13/cobra"

var hypervisorCmd = &cobra.Command{
	Use:   "hypervisor",
	Short: "Run operations related with hypervisors",
}

func init() {
	hypervisorCmd.AddCommand(hypervisorListCmd)
}
