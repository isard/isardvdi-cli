package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var desktopStartCmd = &cobra.Command{
	Use:   "start",
	Short: "start a desktop",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := cmd.Flag("id").Value.String()

		if err := cli.DesktopStart(cmd.Context(), id); err != nil {
			return err
		}

		fmt.Printf("Desktop %s started\n", id)

		return nil
	},
}

func init() {
	desktopStartCmd.Flags().StringP("id", "i", "", "ID of the desktop")
	desktopStartCmd.MarkFlagRequired("id")
}
