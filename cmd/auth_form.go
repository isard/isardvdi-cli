package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/cfg"
	"golang.org/x/term"
)

var authFormCmd = &cobra.Command{
	Use:   "form",
	Short: "Login using the form provider",
	RunE: func(cmd *cobra.Command, args []string) error {
		if cmd.Flag("username").Value.String() == "" {
			fmt.Print("Enter username: ")
			r := bufio.NewReader(os.Stdin)
			usr, err := r.ReadString('\n')
			if err != nil {
				return fmt.Errorf("read username from stdin: %w", err)
			}

			cmd.Flag("username").Value.Set(strings.TrimSpace(usr))
		}

		if cmd.Flag("password").Value.String() == "" {
			fmt.Print("Enter password: ")
			pwd, err := term.ReadPassword(int(syscall.Stdin))
			if err != nil {
				return fmt.Errorf("read password from stdin: %w", err)
			}

			fmt.Println()

			cmd.Flag("password").Value.Set(strings.TrimSpace(string(pwd)))
		}

		tkn, err := cli.AuthForm(cmd.Context(), cmd.Flag("category").Value.String(), cmd.Flag("username").Value.String(), cmd.Flag("password").Value.String())

		config.Token = tkn
		if cfgErr := cfg.Save(config); cfgErr != nil {
			return fmt.Errorf("write token to the configuration file: %w", cfgErr)
		}

		return err
	},
}

func init() {
	authFormCmd.Flags().StringP("category", "c", "default", "Category ID of the user")
	authFormCmd.Flags().StringP("username", "u", "", "Username")
	authFormCmd.Flags().StringP("password", "p", "", "Password")
}
