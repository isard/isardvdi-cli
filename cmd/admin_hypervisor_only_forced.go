package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

var adminHypervisorOnlyForcedCmd = &cobra.Command{
	Use:   "only-forced",
	Short: "set the only forced state of a hypervisor",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := cmd.Flag("id").Value.String()
		state, err := strconv.ParseBool(cmd.Flag("state").Value.String())
		if err != nil {
			return fmt.Errorf("parse hypervisor state flag: %w", err)
		}

		if err := cli.AdminHypervisorOnlyForced(cmd.Context(), id, state); err != nil {
			return err
		}

		return nil
	},
}

func init() {
	adminHypervisorOnlyForcedCmd.Flags().StringP("id", "i", "", "ID of the hypervisor")
	adminHypervisorOnlyForcedCmd.MarkFlagRequired("id")
	adminHypervisorOnlyForcedCmd.Flags().StringP("state", "s", "false", "Whether the hypervisor is only forced. Accepted vaules are 'true' or 'false'")
	adminHypervisorOnlyForcedCmd.MarkFlagRequired("state")
}
