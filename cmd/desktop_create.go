package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

var desktopCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "create desktop(s) from a template",
	RunE: func(cmd *cobra.Command, args []string) error {
		name := cmd.Flag("name").Value.String()
		templateID := cmd.Flag("from-template").Value.String()

		d, err := cli.DesktopCreate(cmd.Context(), name, templateID)
		if err != nil {
			return err
		}

		fmt.Printf("Desktop %s created\n", client.GetString(d.ID))
		return nil
	},
}

func init() {
	desktopCreateCmd.Flags().StringP("name", "n", "", "Name of the desktop")
	desktopCreateCmd.MarkFlagRequired("name")
	desktopCreateCmd.Flags().StringP("from-template", "t", "", "ID of the desktop template")
	desktopCreateCmd.Flags().Bool("volatile", false, "TO IMPLEMENT: Sets if the desktop is going to be volatile")
}
