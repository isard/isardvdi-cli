package cmd

import "github.com/spf13/cobra"

var adminHypervisorCmd = &cobra.Command{
	Use:   "hypervisor",
	Short: "Run hypervisor management operations",
}

func init() {
	adminHypervisorCmd.AddCommand(adminHypervisorOnlyForcedCmd)
}
