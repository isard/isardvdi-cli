package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var adminUserDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a user",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := cmd.Flag("id").Value.String()

		if err := cli.AdminUserDelete(cmd.Context(), id); err != nil {
			return err
		}

		fmt.Printf("User '%s' deleted\n", id)

		return nil
	},
}

func init() {
	adminUserDeleteCmd.Flags().StringP("id", "i", "", "ID of the user")
	adminUserDeleteCmd.MarkFlagRequired("id")
}
