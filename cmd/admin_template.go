package cmd

import "github.com/spf13/cobra"

var adminTemplateCmd = &cobra.Command{
	Use:   "template",
	Short: "Run template management operations",
}

func init() {
	adminTemplateCmd.AddCommand(adminTemplateListCmd)
}
