package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var vpnCmd = &cobra.Command{
	Use:   "vpn",
	Short: "get the VPN configuration for the user",
	RunE: func(cmd *cobra.Command, args []string) error {
		content, err := cli.UserVPN(cmd.Context())
		if err != nil {
			return err
		}

		fmt.Println(content)

		return nil
	},
}
