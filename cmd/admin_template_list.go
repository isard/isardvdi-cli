package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"

	"github.com/olekukonko/tablewriter"
)

var adminTemplateListCmd = &cobra.Command{
	Use:   "list",
	Short: "list all the templates",
	RunE: func(cmd *cobra.Command, args []string) error {
		templates, err := cli.AdminTemplateList(cmd.Context())
		if err != nil {
			return err
		}

		if outputJSON {
			return encodeJSON(templates)
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name"})

		for _, t := range templates {
			table.Append([]string{client.GetString(t.ID), client.GetString(t.Name)})
		}

		table.Render()

		return nil
	},
}
