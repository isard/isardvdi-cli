package cmd

import (
	"os"

	"gitlab.com/isard/isardvdi-cli/pkg/client"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

var adminUserListCmd = &cobra.Command{
	Use:   "list",
	Short: "list all the users",
	RunE: func(cmd *cobra.Command, args []string) error {
		users, err := cli.AdminUserList(cmd.Context())
		if err != nil {
			return err
		}

		if outputJSON {
			return encodeJSON(users)
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name", "Provider", "Role", "Category", "Group", "UID", "Username"})

		for _, u := range users {
			table.Append([]string{client.GetString(u.ID), client.GetString(u.Name), client.GetString(u.Provider), client.GetString(u.Role), client.GetString(u.Category), client.GetString(u.Group), client.GetString(u.UID), client.GetString(u.Username)})
		}

		table.Render()

		return nil
	},
}
