package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

var desktopViewerCmd = &cobra.Command{
	Use:   "viewer",
	Short: "download a desktop viewer",
	RunE: func(cmd *cobra.Command, args []string) error {
		t := cmd.Flag("type").Value.String()
		id := cmd.Flag("id").Value.String()

		content, err := cli.DesktopViewer(cmd.Context(), client.DesktopViewer(t), id)
		if err != nil {
			return err
		}

		fmt.Println(content)

		return nil
	},
}

func init() {
	desktopViewerCmd.Flags().StringP("type", "t", "spice", `The viewer type. Supported types: "spice", "rdp"`)
	desktopViewerCmd.Flags().StringP("id", "i", "", "ID of the desktop")
	desktopViewerCmd.MarkFlagRequired("id")
}
