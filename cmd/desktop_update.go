package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

var desktopUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "update a desktop",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := cmd.Flag("id").Value.String()
		var forcedHyp string
		if cmd.Flag("forced-hyp").Value != nil {
			forcedHyp = cmd.Flag("forced-hyp").Value.String()
		}

		opts := client.DesktopUpdateOptions{
			ForcedHyp: []string{forcedHyp},
		}

		fmt.Println(id)

		if err := cli.DesktopUpdate(cmd.Context(), id, opts); err != nil {
			return err
		}

		fmt.Printf("Desktop %s updated\n", id)

		return nil
	},
}

func init() {
	desktopUpdateCmd.Flags().StringP("id", "i", "", "ID of the desktop")
	desktopUpdateCmd.MarkFlagRequired("id")
	desktopUpdateCmd.Flags().String("forced-hyp", "", "ID of the hypervisor where the desktop is going to be forced to start")
}
