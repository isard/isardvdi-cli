package cmd

import "github.com/spf13/cobra"

var desktopCmd = &cobra.Command{
	Use:   "desktop",
	Short: "Run operations related with desktops",
}

func init() {
	desktopCmd.AddCommand(desktopListCmd)
	desktopCmd.AddCommand(desktopGetCmd)
	desktopCmd.AddCommand(desktopCreateCmd)
	desktopCmd.AddCommand(desktopDeleteCmd)
	desktopCmd.AddCommand(desktopStartCmd)
	desktopCmd.AddCommand(desktopStopCmd)
	desktopCmd.AddCommand(desktopViewerCmd)
	desktopCmd.AddCommand(desktopUpdateCmd)
}
