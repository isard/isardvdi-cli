package cmd

import "github.com/spf13/cobra"

var adminDesktopCmd = &cobra.Command{
	Use:   "desktop",
	Short: "Run desktop management operations",
}

func init() {
	adminDesktopCmd.AddCommand(adminDesktopListCmd)
}
