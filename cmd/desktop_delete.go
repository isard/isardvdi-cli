package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var desktopDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a desktop",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := cmd.Flag("id").Value.String()

		if err := cli.DesktopDelete(cmd.Context(), id); err != nil {
			return err
		}

		fmt.Printf("Desktop %s deleted\n", id)
		return nil
	},
}

func init() {
	desktopDeleteCmd.Flags().StringP("id", "i", "", "ID of the desktop")
	desktopDeleteCmd.MarkFlagRequired("id")
}
