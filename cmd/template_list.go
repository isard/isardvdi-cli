package cmd

import (
	"os"

	"gitlab.com/isard/isardvdi-cli/pkg/client"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

var templateListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all the templates of the user",
	RunE: func(cmd *cobra.Command, args []string) error {
		templates, err := cli.TemplateList(cmd.Context())
		if err != nil {
			return err
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name"})

		for _, t := range templates {
			table.Append([]string{client.GetString(t.ID), client.GetString(t.Name)})
		}

		table.Render()

		return nil
	},
}
