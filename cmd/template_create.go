package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

var templateCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "create a tempalte from a desktop",
	RunE: func(cmd *cobra.Command, args []string) error {
		name := cmd.Flag("name").Value.String()
		desktopID := cmd.Flag("from-desktop").Value.String()

		t, err := cli.TemplateCreateFromDesktop(cmd.Context(), name, desktopID)
		if err != nil {
			return err
		}

		fmt.Println(client.GetString(t.ID))

		return nil
	},
}

func init() {
	templateCreateCmd.Flags().StringP("name", "n", "", "Name of the template")
	templateCreateCmd.MarkFlagRequired("name")
	templateCreateCmd.Flags().StringP("from-desktop", "t", "", "ID of the desktop")
}
