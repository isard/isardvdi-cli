package cmd

import "github.com/spf13/cobra"

var adminCmd = &cobra.Command{
	Use:   "admin",
	Short: "Run administration operations",
}

func init() {
	adminCmd.AddCommand(adminUserCmd)
	adminCmd.AddCommand(adminDesktopCmd)
	adminCmd.AddCommand(adminTemplateCmd)
	adminCmd.AddCommand(adminHypervisorCmd)
}
