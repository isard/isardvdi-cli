package cmd

import (
	"fmt"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
	"golang.org/x/term"
)

var adminUserCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "create a user",
	RunE: func(cmd *cobra.Command, args []string) error {
		provider := cmd.Flag("provider").Value.String()
		role := cmd.Flag("role").Value.String()
		category := cmd.Flag("category").Value.String()
		group := cmd.Flag("group").Value.String()
		usr := cmd.Flag("username").Value.String()

		if cmd.Flag("password").Value.String() == "" {
			fmt.Print("Enter password: ")
			pwd, err := term.ReadPassword(int(syscall.Stdin))
			if err != nil {
				return fmt.Errorf("read password from stdin: %w", err)
			}

			fmt.Println()

			cmd.Flag("password").Value.Set(strings.TrimSpace(string(pwd)))
		}

		u, err := cli.AdminUserCreate(cmd.Context(), provider, role, category, group, usr, usr, cmd.Flag("password").Value.String())
		if err != nil {
			return err
		}

		fmt.Printf("User '%s' created\n", client.GetString(u.ID))
		return nil
	},
}

func init() {
	adminUserCreateCmd.Flags().String("provider", "local", "The provider ID of the user")
	adminUserCreateCmd.Flags().StringP("role", "r", "user", "The role ID of the user")
	adminUserCreateCmd.Flags().StringP("category", "c", "default", "The category ID of the user")
	adminUserCreateCmd.Flags().StringP("group", "g", "default-default", "The group ID of the user")
	adminUserCreateCmd.Flags().StringP("username", "u", "", "Username")
	adminUserCreateCmd.MarkFlagRequired("username")
	adminUserCreateCmd.Flags().StringP("password", "p", "", "Password")
}
