package cmd

import "github.com/spf13/cobra"

var adminUserCmd = &cobra.Command{
	Use:   "user",
	Short: "Run user management operations",
}

func init() {
	adminUserCmd.AddCommand(adminUserListCmd)
	adminUserCmd.AddCommand(adminUserCreateCmd)
	adminUserCmd.AddCommand(adminUserDeleteCmd)
}
