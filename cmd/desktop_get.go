package cmd

import (
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

var desktopGetCmd = &cobra.Command{
	Use:   "get",
	Short: "get information about a desktop",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := cmd.Flag("id").Value.String()

		d, err := cli.DesktopGet(cmd.Context(), id)
		if err != nil {
			return err
		}

		if outputJSON {
			return encodeJSON(d)
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name", "State", "IP"})
		table.Append([]string{client.GetString(d.ID), client.GetString(d.Name), client.GetString(d.State), client.GetString(d.IP)})
		table.Render()

		return nil
	},
}

func init() {
	desktopGetCmd.Flags().StringP("id", "i", "", "ID of the desktop")
	desktopGetCmd.MarkFlagRequired("id")
}
