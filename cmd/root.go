package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/isard/isardvdi-cli/pkg/cfg"
	"gitlab.com/isard/isardvdi-cli/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cli        *client.Client
	config     = &cfg.Cfg{}
	outputJSON bool
	rootCmd    = &cobra.Command{
		Use:     "isardvdi-cli",
		Short:   "IsardVDI is a command line tool for interacting with IsardVDI",
		Version: client.Version,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
			cli, err = client.NewClient(config)
			if err != nil {
				return err
			}

			return nil
		},
	}
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(onInit)

	rootCmd.PersistentFlags().String("host", "", "IsardVDI host (https://isardvdi.com)")
	viper.BindPFlag("host", rootCmd.PersistentFlags().Lookup("host"))

	rootCmd.PersistentFlags().BoolVar(&outputJSON, "json", false, "Show the output in JSON format instead of tables")

	rootCmd.AddCommand(authCmd)
	rootCmd.AddCommand(vpnCmd)
	rootCmd.AddCommand(desktopCmd)
	rootCmd.AddCommand(templateCmd)
	rootCmd.AddCommand(hypervisorCmd)
	rootCmd.AddCommand(adminCmd)
}

func onInit() {
	cfg.Init(config)
}

func encodeJSON(v interface{}) error {
	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "	")
	return enc.Encode(v)
}
