package cmd

import (
	"os"
	"strconv"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/isard/isardvdi-cli/pkg/client"
)

var hypervisorListCmd = &cobra.Command{
	Use:   "list",
	Short: "list all the hypervisors in the system",
	RunE: func(cmd *cobra.Command, args []string) error {
		hypervisors, err := cli.HypervisorList(cmd.Context())
		if err != nil {
			return err
		}

		if outputJSON {
			return encodeJSON(hypervisors)
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "URI", "Status", "Only Forced", "Buffering Hypervisor"})

		for _, h := range hypervisors {
			table.Append([]string{client.GetString(h.ID), client.GetString(h.URI), string(*h.Status), strconv.FormatBool(client.GetBool(h.OnlyForced)), strconv.FormatBool(client.GetBool(h.Buffering))})
		}

		table.Render()

		return nil
	},
}
