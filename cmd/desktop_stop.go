package cmd

import "github.com/spf13/cobra"

var desktopStopCmd = &cobra.Command{
	Use:   "stop",
	Short: "stop a desktop",
	RunE: func(cmd *cobra.Command, args []string) error {
		id := cmd.Flag("id").Value.String()

		return cli.DesktopStop(cmd.Context(), id)
	},
}

func init() {
	desktopStopCmd.Flags().StringP("id", "i", "", "ID of the desktop")
	desktopStopCmd.MarkFlagRequired("id")
}
