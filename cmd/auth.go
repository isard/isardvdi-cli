package cmd

import "github.com/spf13/cobra"

var authCmd = &cobra.Command{
	Use:   "auth",
	Short: "Run operations related with authentication",
}

func init() {
	authCmd.AddCommand(authFormCmd)
}
